﻿using System;
using System.Collections;
using System.Collections.Generic;
using TurboEncapulator.Backend;
using TurboEncapulator.Viewer;
using UniRx;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace TurboEncapulator.Debugging
{
    /// <summary>
    /// This emulates a client receiving updated metric data from a server
    /// </summary>
    public class MetricEmulator : MonoBehaviour
    {
        [System.Serializable]
        private class EmulationParams
        {
            public float maxValue;
            public float minValue;
            public float maxDelta;
            public MetricMetadata metaData;
        }

        [SerializeField] EmulationParams[] emulationParams;

        [Inject] IMetricRecorder metricRecorder;
        [Inject] ViewerState viewerState;

        private void Start()
        {
            // Wait until its done loading to start the emulation
            viewerState.isLoading
                .TakeUntilDestroy(this)
                .Where(isLoading => isLoading)
                .Subscribe(_ => Init());
        }

        private void Init()
        {
            // Set up all the metrics to update randomly every second
            foreach (var p in emulationParams)
            {
                metricRecorder.SetMetricMetadata(p.metaData);

                var currentValue = Mathf.Lerp(p.minValue, p.maxValue, .5f);

                Observable.Interval(TimeSpan.FromSeconds(1))
                    .TakeUntilDestroy(this)
                    .Subscribe(_ =>
                    {
                        var delta = Random.Range(-p.maxDelta, p.maxDelta);
                        currentValue += delta;
                        currentValue = Mathf.Clamp(currentValue, p.minValue, p.maxValue);

                        metricRecorder.AddMetricData(p.metaData.id, currentValue);
                    });
            }

            // Keep track of how many metric updates received since launch to keep the time index up to date
            var counter = 0;
            Observable.Interval(TimeSpan.FromSeconds(1))
                .TakeUntilDestroy(this)
                .Subscribe(_ =>
                {
                    counter++;
                    if (!viewerState.isPaused.Value)
                        viewerState.timeIndex.Value = counter;
                });

            // Update the time index to latest once unpaused
            viewerState.isPaused
                .TakeUntilDestroy(this)
                .Where(isPaused => !isPaused)
                .Subscribe(_ => viewerState.timeIndex.Value = counter);
        }
    }
}