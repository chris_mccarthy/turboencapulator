﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace TurboEncapulator.Backend
{
    /// <summary>
    /// This is a basic repository for tracking remote data
    /// </summary>
    public interface IRepository<T>
    {
        /// <summary>
        /// The data saved to the repo. Use Linq to find the element you want
        /// </summary>
        IReadOnlyReactiveProperty<IEnumerable<T>> Data { get; }
    }
}