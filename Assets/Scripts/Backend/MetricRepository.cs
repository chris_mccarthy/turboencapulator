﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using Zenject;

namespace TurboEncapulator.Backend
{
    /// <summary>
    /// Meta data about metric data
    /// </summary>
    [System.Serializable]
    public class MetricMetadata
    {
        /// <summary>
        /// The id of the metric data ie MPH, MPG, RPM
        /// </summary>
        public string id;
        /// <summary>
        /// The maximum this value can be
        /// </summary>
        public float max;
        /// <summary>
        /// Will the warning state be entered from being too high?
        /// </summary>
        public bool warningHigh = true;
        /// <summary>
        /// The value that must be surpassed to enter a warning state
        /// </summary>
        public float warningValue;
        /// <summary>
        /// The lable displayed to the user to denote the unit
        /// </summary>
        public string unitLabel;
    }

    /// <summary>
    /// A record of all received metric data
    /// </summary>
    public class MetricData
    {
        /// <summary>
        /// The values recorded to the metric
        /// </summary>
        public List<float> values = new List<float>();
        /// <summary>
        /// Metadata about the metric
        /// </summary>
        public MetricMetadata metaData = new MetricMetadata();
    }

    /// <summary>
    /// This provides a realtime archieve of all metric data received by the client
    /// </summary>
    public interface IMetricRepository : IRepository<MetricData> { }

    /// <summary>
    /// This is means of recording metric data from a source
    /// </summary>
    public interface IMetricRecorder
    {
        /// <summary>
        /// Adds or replaces a piece of metric metadata in the repo
        /// </summary>
        void SetMetricMetadata(MetricMetadata metadata);
        /// <summary>
        /// Adds a new value to a metric's data
        /// </summary>
        /// <param name="id">The Id of the metric</param>
        /// <param name="value">The piece of data added</param>
        void AddMetricData(string id, float value);
    }

    public class MetricRepository : MonoInstaller, IMetricRepository, IMetricRecorder
    {
        private ReactiveProperty<IEnumerable<MetricData>> data = new ReactiveProperty<IEnumerable<MetricData>>(new List<MetricData>());
        public IReadOnlyReactiveProperty<IEnumerable<MetricData>> Data => data;

        public void SetMetricMetadata(MetricMetadata metadata)
        {
            var currentDataList = data.Value.ToList();
            var existingData = currentDataList.FirstOrDefault(d => d.metaData.id == metadata.id);

            // If the data doesnt exist, add it
            if (existingData == null)
            {
                currentDataList.Add(new MetricData
                {
                    metaData = metadata
                });
            }
            // If the data does exist, replace it
            else
            {
                currentDataList.Remove(existingData);
                currentDataList.Add(new MetricData
                {
                    metaData = metadata,
                    values = existingData.values
                });
            }
            data.Value = currentDataList;
        }

        public void AddMetricData(string id, float value)
        {
            var currentDataList = data.Value.ToList();

            var metric = currentDataList.FirstOrDefault(d => d.metaData.id == id);
            if (metric != null)
                metric.values.Add(value);

            data.Value = currentDataList;
        }

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<MetricRepository>()
                .FromInstance(this)
                .AsSingle();
        }
    }
}