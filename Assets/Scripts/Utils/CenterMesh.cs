﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TurboEncapulator.Utils
{
    // HACK: This is another tool added en lieu of a mesh editor. This centers the mesh 
    // around the renderer center. This is useful for fixing out z-sorting issues
    [RequireComponent(typeof(MeshFilter))]
    public class CenterMesh : MonoBehaviour
    {
        private void Awake()
        {
            var meshFilter = GetComponent<MeshFilter>();
            var mesh = meshFilter.mesh;
            var verticies = mesh.vertices;

            // Record the position of the verticeies in global space
            for (int a = 0; a < verticies.Length; a++)
            {
                verticies[a] = transform.TransformPoint(verticies[a]);
            }

            // Move the object to its visual center
            var center = transform.GetRenderedBounds().center;
            transform.position = center;

            // Move the verticies to their position in local space
            for (int a = 0; a < verticies.Length; a++)
            {
                verticies[a] = transform.InverseTransformPoint(verticies[a]);
            }

            mesh.vertices = verticies;

            meshFilter.mesh = mesh;

            var collider = GetComponent<MeshCollider>();
            if (collider != null)
                collider.sharedMesh = mesh;
        }
    }
}