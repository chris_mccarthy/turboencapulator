﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TurboEncapulator.Utils
{
    public static class CameraUtils
    {
        /// <summary>
        /// Gets the bounds of an object beased off its renderers
        /// </summary>
        /// <returns>The rendered bounds.</returns>
        public static Bounds GetRenderedBounds(this Transform target)
        {
            Bounds bounds = new Bounds(target.position, Vector3.zero);

            foreach (var renderer in target.gameObject.GetComponentsInChildren<Renderer>())
            {
                var childBounds = renderer.bounds;
                bounds.Encapsulate(childBounds.min);
                bounds.Encapsulate(childBounds.max);
            }

            return bounds;
        }
    }
}