﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace TurboEncapulator.Utils
{
    // HACK: The normals are reversed on one of the model pieces. This should be 
    // fixed in the asset but I dont have room on my computer to install a 3D modeling program
    [RequireComponent(typeof(MeshFilter))]
    public class ReverseNormals : MonoBehaviour
    {
        private void Awake()
        {
            var meshFilter = GetComponent<MeshFilter>();
            var mesh = meshFilter.mesh;

            var triangles = mesh.triangles.ToArray();
            for (int i = 0; i < Mathf.FloorToInt(mesh.triangles.Length) / 3; i++)
            {
                var a = i * 3;
                var b = a + 1;
                var c = a + 2;

                triangles[a] = mesh.triangles[c];
                triangles[b] = mesh.triangles[b];
                triangles[c] = mesh.triangles[a];
            }

            mesh.triangles = triangles;

            mesh.RecalculateNormals();
            meshFilter.mesh = mesh;

            var collider = GetComponent<MeshCollider>();
            if (collider != null)
                collider.sharedMesh = mesh;
        }
    }
}