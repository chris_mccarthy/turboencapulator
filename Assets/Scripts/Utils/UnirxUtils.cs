﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TurboEncapulator.Utils
{
    public static class UnirxUtils
    {
        /// <summary>
        /// Observes various events recieved by an event trigger
        /// </summary>
        /// <returns>A stream of when this event occurs</returns>
        /// <param name="triggerType">The type of event to subscribe to</param>
        public static IObservable<PointerEventData> ObserveTriggerEvent(this EventTrigger trigger, EventTriggerType triggerType)
        {
            EventTrigger.Entry entry = new EventTrigger.Entry
            {
                eventID = triggerType
            };

            return Observable.FromEvent<PointerEventData>(
                action =>
                {
                    entry.callback.AddListener((data) => action((PointerEventData)data));
                    trigger.triggers.Add(entry);
                },
                action =>
                {
                    entry.callback = new EventTrigger.TriggerEvent();
                    Observable.EveryEndOfFrame()
                        .First()
                        .Subscribe(_ => trigger.triggers.Remove(entry));
                });
        }
    }
}