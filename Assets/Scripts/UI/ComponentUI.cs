﻿using System.Collections;
using System.Collections.Generic;
using TurboEncapulator.Viewer;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TurboEncapulator.UI
{
    /// <summary>
    /// A simple UI that displays the data from a selected component
    /// </summary>
    public class ComponentUI : MonoBehaviour
    {
        [SerializeField] Text text;
        [SerializeField] GameObject visualRoot;

        [Inject] ViewerState viewerState;

        private void Start()
        {
            // Show when there is a selected component
            viewerState.selectedComponent
                .TakeUntilDestroy(this)
                .Select(c => c != null)
                .Subscribe(visualRoot.SetActive);

            // Show the text of the selected component
            viewerState.selectedComponent
                .TakeUntilDestroy(this)
                .Where(c => c != null)
                .Select(c => c.DescriptionText)
                .Subscribe(t => text.text = t);
        }
    }
}
