﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TurboEncapulator.Backend;
using TurboEncapulator.Utils;
using TurboEncapulator.Viewer;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace TurboEncapulator.UI
{
    public class BarGraphUI : MonoBehaviour
    {
        [SerializeField] GameObject visualRoot;
        [SerializeField] Transform barParent;
        [SerializeField] GameObject elementPrefab;
        [SerializeField] ScrollRect scrollRect;
        [SerializeField] Scrollbar scrollbar;
        [SerializeField] Toggle latestToggle;
        [SerializeField] Transform labelParent;
        [SerializeField] Button playButton;
        [SerializeField] Text unitLabel;
        [SerializeField] EventTrigger[] deselectTriggers;

        [Inject] IMetricRepository metricRepository;
        [Inject] ViewerState viewerState;
        [Inject] DiContainer container;

        ReactiveProperty<bool> latestEnabled = new ReactiveProperty<bool>(true);
        CompositeDisposable elementDisposables = new CompositeDisposable();

        private void Start()
        {
            var elementPool = new List<GameObject>();

            metricRepository.Data
                .TakeUntilDestroy(this)
                // Use the selected metric id to find the metric data you want 
                .CombineLatest(viewerState.selectedMetricId, (d, metricId) => d.FirstOrDefault(m => m.metaData.id == metricId))
                .Subscribe(metricData =>
                {
                    // Only show the root if there is data to show
                    visualRoot.SetActive(metricData != null);

                    if (metricData != null)
                    {
                        // Dispose of all the element's behavior as they will be recreated in the refresh
                        elementDisposables.Clear();

                        // Set the unit lable text
                        unitLabel.text = metricData.metaData.unitLabel;

                        var values = metricData.values;

                        // Add all the elements to the pool as needed
                        while (elementPool.Count < values.Count)
                        {
                            var barElement = container.InstantiatePrefab(elementPrefab, barParent);
                            elementPool.Add(barElement);
                        }

                        // Remove the elements from the pool as needed
                        while (elementPool.Count > values.Count)
                        {
                            var deleted = elementPool.Last();
                            elementPool.Remove(deleted);
                            Destroy(deleted);
                        }

                        if (values.Any())
                        {
                            // Use the min and max to make it so the bars displayed value is based off of the min and max
                            var max = values.Max();
                            var min = values.Min() - 1;

                            // Refresh every element's data and behavior
                            for (int a = 0; a < elementPool.Count && a < values.Count; a++)
                            {
                                var elementUI = elementPool[a].GetComponent<BarGraphElementUI>();

                                // In order for sorting to work, you have to set a new parent for the bar lables
                                elementUI.Init(new BarGraphElementUI.Config
                                {
                                    lableParent = labelParent
                                });
                                // Set the normalized value to be relative to the min and max
                                elementUI.SetBarValue((values[a] - min) / (max - min));
                                // Set the text value to a rounded integer
                                elementUI.SetValueLabelText(Mathf.RoundToInt(values[a]).ToString());
                                // Show the warning effects if the value is too low or high
                                elementUI.SetWarning(metricData.metaData.warningHigh ? values[a] > metricData.metaData.warningValue : values[a] < metricData.metaData.warningValue);

                                var index = a;
                                // Make it so clicking pauses the updating of the dials and sets the displayed index to that element's index
                                elementUI.OnClick
                                    .TakeUntilDestroy(elementUI)
                                    .Subscribe(_ =>
                                    {
                                        viewerState.isPaused.Value = true;
                                        viewerState.timeIndex.Value = index;
                                    })
                                    .AddTo(elementDisposables);

                                // Make its when the game is paused and the time index of this element is the same, it is selected
                                viewerState.timeIndex
                                    .CombineLatest(viewerState.isPaused, Tuple.Create)
                                    .Select(t => t.Item1 == index && t.Item2)
                                    .TakeUntilDestroy(elementUI)
                                    .Subscribe(elementUI.SetSelected)
                                    .AddTo(elementDisposables);
                            }
                        }

                        // When "Show Latest" is enabled, the scroll rect will move forward everytime new data arrives
                        if (latestEnabled.Value)
                            Observable.EveryEndOfFrame()
                                .First()
                                .Subscribe(_ => scrollRect.horizontalNormalizedPosition = 1f);
                    }
                });

            // When you click the play button, the game is unpaused
            playButton.OnClickAsObservable()
                .TakeUntilDestroy(this)
                .Subscribe(_ => viewerState.isPaused.Value = false);

            // When "Show Latest" is toggled, so is latestEnabled
            latestToggle.OnValueChangedAsObservable()
                .TakeUntilDestroy(this)
                .Subscribe(isOn => latestEnabled.Value = isOn);

            // If a drag, a scroll, or any cursor down event is detected, then latestEnabled is deactivated
            scrollRect.OnScrollAsObservable()
                .Merge(scrollRect.OnDragAsObservable())
                .Merge(scrollbar.OnPointerDownAsObservable())
                .TakeUntilDestroy(this)
                .Subscribe(_ => latestEnabled.Value = false);

            // When latestEnabled is toggled, make sure the UI reflects that
            latestEnabled
                .TakeUntilDestroy(this)
                .Subscribe(isOn => latestToggle.isOn = isOn);

            // When pause is toggled, so is latestEnabled
            // Only show the play button when the viewer is paused
            viewerState.isPaused
                .TakeUntilDestroy(this)
                .Subscribe(isPaused =>
                {
                    latestEnabled.Value = !isPaused;
                    playButton.gameObject.SetActive(isPaused);
                });

            // Only show the latestToggle when the viewer is paused
            viewerState.isPaused
                .TakeUntilDestroy(this)
                .Select(paused => !paused)
                .Subscribe(latestToggle.gameObject.SetActive);

            // When the game is unpaused or the latest is enabled, set the scroll to 1
            viewerState.isPaused
                .Select(paused => !paused)
                .Merge(latestEnabled)
                .Where(isUnpaused => isUnpaused)
                .TakeUntilDestroy(this)
                .Subscribe(_ => scrollRect.horizontalNormalizedPosition = 1f);

            // When the user clicks off of the graph, deselect the metric
            foreach (var t in deselectTriggers)
            {
                t.ObserveTriggerEvent(EventTriggerType.PointerClick)
                    .TakeUntilDestroy(this)
                    .Where(e => !e.dragging)
                    .Subscribe(_ => viewerState.selectedMetricId.Value = string.Empty);
            }
        }

        private void OnDestroy()
        {
            elementDisposables.Dispose();
        }
    }
}