﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TurboEncapulator.Backend;
using TurboEncapulator.Viewer;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TurboEncapulator.UI
{
    public class DialUI : MonoBehaviour
    {
        [SerializeField] string metricId;
        [SerializeField] Image dialImage;
        [SerializeField] Text valueText;
        [SerializeField] Text unitText;
        [SerializeField] Color defaultColor;
        [SerializeField] Color warningColor;
        [SerializeField] Button button;

        [Inject] IMetricRepository metricRepository;
        [Inject] ViewerState viewerState;

        private void Start()
        {
            var goalFill = 0f;
            var currentFill = 0f;
            var fillVelocity = 0f;
            var showWarning = false;

            // Show the metric data based off the current time index
            metricRepository.Data
                .TakeUntilDestroy(this)
                .Select(d => d.FirstOrDefault(m => m.metaData.id == metricId))
                .CombineLatest(viewerState.timeIndex, Tuple.Create)
                .Subscribe(tuple =>
                {
                    var metric = tuple.Item1;
                    var index = tuple.Item2;

                    // Display the metric data if there is any
                    if (metric != null)
                    {
                        if (metric.values.Any() && index < metric.values.Count)
                        {
                            var currentValue = metric.values[index];
                            goalFill = Mathf.Lerp(0, .5f, currentValue / metric.metaData.max);
                            valueText.text = Mathf.RoundToInt(currentValue).ToString();
                            unitText.text = metric.metaData.unitLabel;

                            showWarning = (metric.metaData.warningHigh) ? currentValue > metric.metaData.warningValue : currentValue < metric.metaData.warningValue;
                        }
                    }
                    else
                        valueText.text = "NA";
                });

            // Constantly update the widget based off of the latest fill amount
            Observable.EveryUpdate()
                .TakeUntilDestroy(this)
                .Subscribe(_ =>
                {
                    currentFill = Mathf.SmoothDamp(currentFill, goalFill, ref fillVelocity, .25f);
                    dialImage.fillAmount = currentFill;

                    dialImage.color = Color.Lerp(dialImage.color, (showWarning) ? warningColor : defaultColor, .2f);
                });

            // Set the selected metric id when clicked
            button.OnClickAsObservable()
                .TakeUntilDestroy(this)
                .Subscribe(_ => viewerState.selectedMetricId.Value = metricId);
        }
    }
}