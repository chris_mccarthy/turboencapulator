﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace TurboEncapulator.UI
{
    /// <summary>
    /// This is a simple class that just keeps an object pointed towards the camera
    /// </summary>
    public class Billboard : MonoBehaviour
    {
        [SerializeField] Transform billboardTransform;
        [SerializeField] Transform cameraTransform;

        void Start()
        {
            Observable.EveryUpdate()
                .TakeUntilDestroy(this)
                .Subscribe(_ =>
                {
                    billboardTransform.rotation = Quaternion.LookRotation(cameraTransform.forward, cameraTransform.up);
                });
        }
    }
}