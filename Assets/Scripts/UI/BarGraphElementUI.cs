﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UniRx;
using TurboEncapulator.Utils;
using UniRx.Triggers;
using Zenject;
using TurboEncapulator.Viewer;
using System;

namespace TurboEncapulator.UI
{
    public class BarGraphElementUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [System.Serializable]
        public class Config
        {
            public Transform lableParent;
        }

        [SerializeField] Transform barTransform;
        [SerializeField] Image barImage;
        [SerializeField] Transform valueTextPin;
        [SerializeField] Transform valueLabelRoot;
        [SerializeField] Text valueLabelText;
        [SerializeField] Color defaultColor;
        [SerializeField] Color warningColor;
        [SerializeField] Color selectColor;
        [SerializeField] Button button;

        bool showWarning;
        bool isSelected;

        private void Awake()
        {
            // Turn the value label off on launch
            valueLabelRoot.gameObject.SetActive(false);
            valueLabelRoot.position = valueTextPin.position;

            // make it so the label is always at the position of the pin
            Observable.EveryUpdate()
                .TakeUntilDestroy(this)
                .Subscribe(_ => valueLabelRoot.position = valueTextPin.position);
        }

        /// <summary>
        /// The on click callback
        /// </summary>
        public IObservable<Unit> OnClick
        {
            get => button.OnClickAsObservable()
                    .Select(_ => Unit.Default);
        }

        /// <summary>
        /// Configure the UI's references
        /// </summary>
        public void Init(Config config)
        {
            valueLabelRoot.SetParent(config.lableParent);
        }

        /// <summary>
        /// Set the bar's normalized value
        /// </summary>
        public void SetBarValue(float normalizedValue)
        {
            barTransform.localScale = new Vector3(1, normalizedValue, 1);
        }

        /// <summary>
        /// Sets the value label's text
        /// </summary>
        public void SetValueLabelText(string text)
        {
            valueLabelText.text = text;
        }

        /// <summary>
        /// Toggles the warning mode
        /// </summary>
        public void SetWarning(bool showWarning)
        {
            this.showWarning = showWarning;
            RefreshColor();
        }

        /// <summary>
        /// Toggles the selected mode
        /// </summary>
        public void SetSelected(bool isSelected)
        {
            this.isSelected = isSelected;
            RefreshColor();
        }

        private void RefreshColor()
        {
            barImage.color = (isSelected) ? selectColor : ((showWarning) ? warningColor : defaultColor);
        }

        private void OnDestroy()
        {
            Destroy(valueLabelRoot.gameObject);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            valueLabelRoot.gameObject.SetActive(true);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            valueLabelRoot.gameObject.SetActive(false);
        }
    }
}