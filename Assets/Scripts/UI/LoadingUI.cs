﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace TurboEncapulator.UI
{
    /// <summary>
    /// A very simple class that creates the visuals of the loading icon
    /// </summary>
    public class LoadingUI : MonoBehaviour
    {
        [SerializeField] Text loadingText;
        [SerializeField] float loadingTextSpeed = 1f;
        [SerializeField] Transform loadingRoot;
        [SerializeField] float spinningSpeed = 45f;

        private void Start()
        {
            // Spin the icon
            Observable.EveryUpdate()
                .TakeUntilDestroy(this)
                .Subscribe(_ =>
                {
                    loadingRoot.rotation = Quaternion.Euler(0, 0, Time.time * spinningSpeed);
                });

            // Update the loading text
            Observable.Interval(TimeSpan.FromSeconds(loadingTextSpeed))
                .TakeUntilDestroy(this)
                .Subscribe(_ =>
                {
                    var sb = new StringBuilder("Loading");
                    var periodCount = Mathf.FloorToInt(Time.time % 4);
                    for (int a = 0; a < periodCount; a++)
                    {
                        sb.Append('.');
                    }
                    loadingText.text = sb.ToString();
                });
        }
    }
}