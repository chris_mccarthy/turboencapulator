﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TurboEncapulator.Viewer;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Zenject;

namespace TurboEncapulator.UI
{
    /// <summary>
    /// Creates the visuals of the splash screen and emulates the loading process
    /// </summary>
    public class SplashscreenUI : MonoBehaviour
    {
        [System.Serializable]
        private class TweenGroup
        {
            public Transform offscreenPin;
            public Transform movedObject;
        }

        [SerializeField] TweenGroup upGroup;
        [SerializeField] TweenGroup downGroup;

        [Inject] ViewerState viewerState;

        void Start()
        {
            void HideTweenGroup(TweenGroup tweenGroup)
            {
                tweenGroup.movedObject.DOMove(tweenGroup.offscreenPin.position, 1f);
            }

            // Emulate the loading after a certain amount of time
            Observable.Timer(TimeSpan.FromSeconds(3f))
                .First()
                .TakeUntilDestroy(this)
                .Subscribe(_ =>
                {
                    viewerState.isLoading.Value = false;
                });

            // When its done loading, hide the splash screen
            viewerState.isLoading
                .Where(isLoading => !isLoading)
                .TakeUntilDestroy(this)
                .Subscribe(isLoading =>
                {
                    HideTweenGroup(upGroup);
                    HideTweenGroup(downGroup);
                });
        }
    }
}