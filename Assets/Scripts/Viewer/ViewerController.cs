﻿using System.Collections;
using System.Collections.Generic;
using TurboEncapulator.Utils;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace TurboEncapulator.Viewer
{
    // Controls higher level bahviors of the viewer
    public class ViewerController : MonoBehaviour
    {
        [SerializeField] GameObject root;
        [SerializeField] EventTrigger[] deselectTriggers;

        [Inject] ViewerState viewerState;

        void Start()
        {
            // If it is not loading, show the root
            viewerState.isLoading
                .TakeUntilDestroy(this)
                .Select(loading => !loading)
                .Subscribe(root.SetActive);

            foreach (var t in deselectTriggers)
            {
                t.ObserveTriggerEvent(EventTriggerType.PointerClick)
                    .TakeUntilDestroy(this)
                    .Where(e => !e.dragging)
                    .Subscribe(_ => viewerState.selectedComponent.Value = null);
            }
        }
    }
}