﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TurboEncapulator.Utils;
using UniRx;
using UniRx.Diagnostics;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;
using Random = UnityEngine.Random;

namespace TurboEncapulator.Viewer
{
    public interface ISubjectComponent
    {
        string DescriptionText { get; }
    }

    /// <summary>
    /// A part of the subject that can be inspected in the viewer
    /// </summary>
    [RequireComponent(typeof(Outline))]
    public class SubjectComponent : MonoBehaviour, ISubjectComponent, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IPointerDownHandler
    {
        [System.Serializable]
        private class OutlineParams
        {
            public Color color;
            public float width;
            public Outline.Mode mode = Outline.Mode.OutlineAll;
        }

        [SerializeField] bool hasErrorCodes = true;
        [SerializeField] OutlineParams hoverOutline;
        [SerializeField] OutlineParams selectOutline;
        [SerializeField] OutlineParams errorOutline;

        [Inject] ViewerState viewerState;

        public string DescriptionText { get; private set; }

        private bool isHover = false;

        private void Start()
        {
            bool isSelected = false;
            bool isError = false;

            var outline = GetComponent<Outline>();

            // Update the visual based on if the user is hovering, selected, or has an error
            Observable.EveryUpdate()
                .TakeUntilDestroy(this)
                .Subscribe(t =>
                {
                    var parameters = isSelected ? selectOutline : isHover ? hoverOutline : isError && Time.time % 2f <= 1f ? errorOutline : null;

                    if (parameters != null)
                    {
                        outline.enabled = true;

                        outline.OutlineColor = parameters.color;
                        outline.OutlineWidth = parameters.width;
                        outline.OutlineMode = parameters.mode;
                    }
                    else
                        outline.enabled = false;
                });

            // If this is the selected component, select it
            viewerState.selectedComponent
                .TakeUntilDestroy(this)
                .Select(c => c == this)
                .Subscribe(selected => isSelected = selected);

            // Set the description text and record if there is an error
            DescriptionText = GenerateStatusText(ref isError);
        }

        string GenerateStatusText(ref bool hasError)
        {
            var normalColor = "#28AFB0";
            var mediumColor = "#F7B538";
            var highColor = "#D7263D";

            var miles = Mathf.RoundToInt(Random.Range(50000, 100000));
            var maxMiles = Mathf.RoundToInt(Random.Range(80000, 120000));
            var age = Mathf.RoundToInt(Random.Range(12, 48));
            var maxAge = Mathf.RoundToInt(Random.Range(24, 60));

            var statusValue = 0;
            statusValue += (miles > maxMiles) ? 1 : 0;
            statusValue += (age > maxAge) ? 1 : 0;

            hasError = hasErrorCodes && Random.value < .05f;

            var sb = new StringBuilder();
            sb.Append("<size=20>");
            sb.Append(gameObject.name);
            sb.Append(":");
            sb.Append("</size>");

            //Miles
            sb.Append("\n\t- Miles: ");
            sb.Append(miles.ToString());
            sb.Append("/");
            sb.Append(maxMiles.ToString());
            sb.Append(" miles");

            //Age
            sb.Append("\n\t- Age: ");
            sb.Append(age.ToString());
            sb.Append("/");
            sb.Append(age.ToString());
            sb.Append(" months");

            //Age
            sb.Append("\n\t- Status: ");
            if (hasError)
            {
                sb.Append("<color=");
                sb.Append(highColor);
                sb.Append(">");
                sb.Append(Mathf.FloorToInt(Random.value * 10000).ToString());
                sb.Append(" Error</color>");
            }
            else
            {
                var color = string.Empty;
                var statusText = string.Empty;
                switch (statusValue)
                {
                    case 0:
                        color = normalColor;
                        statusText = "Normal";
                        break;
                    case 1:
                        color = mediumColor;
                        statusText = "Medium";
                        break;
                    case 3:
                        color = highColor;
                        statusText = "Bad";
                        break;
                }

                sb.Append("<color=");
                sb.Append(color);
                sb.Append(">");
                sb.Append(statusText);
                sb.Append("</color>");
            }

            return sb.ToString();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            isHover = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            isHover = false;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            viewerState.selectedComponent.Value = this;
        }

        // HACK: For some reason OnPointerClick is not being detected if the class does not implement IPointerDownHandler
        public void OnPointerDown(PointerEventData eventData) { }
    }
}