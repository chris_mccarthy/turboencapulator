﻿using System.Collections;
using System.Collections.Generic;
using TurboEncapulator.Utils;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace TurboEncapulator.Viewer
{
    public class CameraController : MonoBehaviour
    {
        [System.Serializable]
        private class Settings
        {
            // How much the movement in screenspace is multiplied before being applied to rotation
            public Vector2 dragSensitivity = Vector2.one;
            // How much the orbiting is damped
            public float cameraOrbitDamp = 90f;
            // The cap on how far vertically the camera can rotate
            public float maxYOrbit = 80;
            // How much the scroll is multiplied before being applied to the zoom
            public float zoomSensitivity = 1f;
            // The closest the camera can get to the target
            public float minZoom = 10;
            // The furthest the camera can get from the target
            public float maxZoom = 20;
        }

        [SerializeField] Settings settings;
        [SerializeField] Transform cameraTransform;
        [SerializeField] Transform target;
        [SerializeField] EventTrigger[] dragTriggers;

        [Inject] ViewerState viewerState;

        private void Start()
        {
            // Wait until the asset is loaded before initializing behavior
            viewerState.isLoading
                .First(isLoading => !isLoading)
                .TakeUntilDestroy(this)
                .Subscribe(_ => Init());
        }

        void Init()
        {
            var cameraOrbitVelocity = new Vector2();
            var cameraOrbitRotation = new Vector2(-30, 30);
            var cameraGoalOrbitRotation = new Vector2(-30, 30);

            var zoomGoal = Mathf.Lerp(settings.minZoom, settings.maxZoom, .5f);
            var zoomCurrent = settings.maxZoom;
            var zoomVelocity = 0f;

            var center = target.position;
            var centerGoal = target.position;
            var centerVelocity = Vector3.zero;

            // Initialize various behaviors when the user interacts with the drag triggers
            foreach (var t in dragTriggers)
            {
                t.ObserveTriggerEvent(EventTriggerType.Drag)
                    .TakeUntilDestroy(this)
                    .Subscribe(d =>
                    {
                        cameraGoalOrbitRotation += d.delta * settings.dragSensitivity * new Vector2(1, -1);
                        cameraGoalOrbitRotation.y = Mathf.Clamp(cameraGoalOrbitRotation.y, -settings.maxYOrbit, settings.maxYOrbit);
                    });

                t.ObserveTriggerEvent(EventTriggerType.Scroll)
                    .TakeUntilDestroy(this)
                    .Subscribe(s =>
                    {
                        zoomGoal += -s.scrollDelta.y * settings.zoomSensitivity;
                        zoomGoal = Mathf.Clamp(zoomGoal, settings.minZoom, settings.maxZoom);
                    });
            }

            // Apply all damping and updates to the camera's behavior
            Observable.EveryUpdate()
                .TakeUntilDestroy(this)
                .Subscribe(_ =>
                {
                    center = Vector3.SmoothDamp(center, centerGoal, ref centerVelocity, .25f);
                    cameraOrbitRotation = Vector2.SmoothDamp(cameraOrbitRotation, cameraGoalOrbitRotation, ref cameraOrbitVelocity, settings.cameraOrbitDamp);
                    zoomCurrent = Mathf.SmoothDamp(zoomCurrent, zoomGoal, ref zoomVelocity, .25f);

                    var cameraRotation = new Vector3(cameraOrbitRotation.y, cameraOrbitRotation.x, 0);

                    cameraTransform.eulerAngles = cameraRotation;
                    cameraTransform.position = center + Quaternion.Euler(cameraRotation) * Vector3.back * zoomCurrent;
                });

            // Move the camera to center on the selected component
            // NOTE: this behavior was deactivated as it wasnt useful
            /*viewerState.selectedComponent
                .TakeUntilDestroy(this)
                .Subscribe(c =>
                {
                    if (c != null)
                        goalCenter = c.transform.GetRenderedBounds().center;
                    else
                        goalCenter = target.position;
                });*/
        }
    }
}