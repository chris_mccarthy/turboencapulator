﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using Zenject;

namespace TurboEncapulator.Viewer
{
    /// <summary>
    /// A centralized location for tracking and subscribing to the state of the viewer
    /// </summary>
    public class ViewerState : MonoInstaller
    {
        /// <summary>
        /// Is the viewer loading
        /// </summary>
        public ReactiveProperty<bool> isLoading = new ReactiveProperty<bool>(true);
        /// <summary>
        /// The id of the selected metric
        /// </summary>
        public ReactiveProperty<string> selectedMetricId = new ReactiveProperty<string>();
        /// <summary>
        /// The index of the currently inspected timestamp
        /// </summary>
        public ReactiveProperty<int> timeIndex = new ReactiveProperty<int>();
        /// <summary>
        /// Is the viewer paused
        /// </summary>
        public ReactiveProperty<bool> isPaused = new ReactiveProperty<bool>(false);
        /// <summary>
        /// The component of the subject currently selected
        /// </summary>
        public ReactiveProperty<ISubjectComponent> selectedComponent = new ReactiveProperty<ISubjectComponent>();

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<ViewerState>()
                .FromInstance(this)
                .AsSingle();
        }
    }
}